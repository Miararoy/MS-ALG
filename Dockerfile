FROM python:3.6.5-slim-stretch

COPY ./metric_classification ./metric_classification/
WORKDIR /metric_classification
RUN ls -la
RUN apt-get update && apt-get install -y tk-dev && rm -r /var/lib/apt/lists/*
RUN pip install -r requirements.txt

CMD [ "python", "mc.py" ]
