import numpy as np
from scipy.fftpack import fft, fftfreq
import matplotlib.pyplot as plt

def sigmoid(x):
    return 1 / (1 + np.exp((-1) * x))

x = np.array([i for i in range(-50, 50, 1)])
y = np.array([sigmoid(t) for t in x])

fx = fft(y)
freq = fftfreq(len(x))


plt.plot(freq, fx.real, color='blue')
plt.plot(freq, fx.imag, color='red')
plt.legend(['real', 'imaginary'])
plt.show()