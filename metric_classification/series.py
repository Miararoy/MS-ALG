import itertools
from collections import namedtuple
import matplotlib.pyplot as plt
from fitting.fourier import fourier
from fitting.fit_data import fit_linear
import numpy as np

Dataenrty = namedtuple('Dataentry', ["x", "y"])

class Series(object):
    def __init__(self, series_name, series_type, x, y):
        self.name = series_name
        self.type = series_type
        self.data = Dataenrty(x, y)
        self._class = None
        
    def classify(self):
        fft, freq = fourier(self.data.y)
        num_of_sign = len(list(itertools.groupby(fft.real, lambda x: x > 0)))
        if num_of_sign <= 3:
            self._class = "step"
        else:
            coef, mse, pred = fit_linear(self.data.x, self.data.y)
            if coef[0][0] > 0.5:
                self._class = "trnd"
            else:
                self._class = "cnst"

    def plot(self, _pred=None, _fourier=None, _fourier_freq=None):
        plt.scatter(self.data.x, self.data.y)
        plt.title("{type} plot: {name}".format(
            type=self.type,
            name=self.name
        ))
        if _pred is not None:
            plt.plot(self.data.x, _pred, color='red')
        if _fourier is not None and _fourier_freq is not None:
            plt.plot(_fourier_freq, _fourier, color='red')
        plt.show()
