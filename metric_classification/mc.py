import random
import tabulate
import pandas as pd
from series import Series
from preprocessing.preprocess import normalize, process_series
from preprocessing.moving_average import EWM
from fitting.fit_data import fit_linear, fit_sigomid
from fitting.fourier import fourier

# CONSTANTS
INDEX_COLUMN = 'timestamp'

# LOAD DATA
df_step_matrix = pd.read_csv("data/step_metric_examples.csv")
series_columns_step = [col for col in df_step_matrix.columns if col != INDEX_COLUMN]
df_cnst_matrix = pd.read_csv("data/constant_metric_examples.csv")
series_columns_cnst = [col for col in df_cnst_matrix.columns if col != INDEX_COLUMN]

# preprocess all data
all_data_step = {
    "s_" + k: process_series(df_step_matrix, INDEX_COLUMN, k, "step") for k in series_columns_step
}
all_data_cnst = {
    "c_" + k: process_series(df_cnst_matrix, INDEX_COLUMN, k, "cnst") for k in series_columns_cnst
}
all_data = {**all_data_cnst, **all_data_step}

results = []
for k, s in all_data.items():
    s.classify() # O(nlogn)
    results.append([k, s._class, s.type])

print(tabulate.tabulate(
    tabular_data=results,
    headers=(
        'series name',
        'series predicted class',
        'series actual class',
    ),
    tablefmt='fancy_grid',
))
