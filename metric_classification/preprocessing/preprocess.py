import numpy as np
import pandas as pd
from .moving_average import EWM
from series import Series

MIN_WINDOW_SIZE = 3


def _normalize(_mean, _max, _min):
    return lambda x: (x - _mean) / (_max - _min)


def normalize(data, column):
    """
    data is assumed to have index column, timestamp column and metric column
    """
    columns = list(data.columns)
    if len(columns) == 2:
        x = data.index.values.reshape(len(data.index), 1)[MIN_WINDOW_SIZE * 3:] # O(n)
        x = np.vectorize(_normalize(x.mean(), x.max(), x.min()))(x) # O(n)
        y = data[column].values.reshape(len(data.index), 1)[MIN_WINDOW_SIZE * 3:] # O(n)
        y = np.vectorize(_normalize(y.mean(), y.max(), y.min()))(y) # O(n)
        return x, y
    else:
        raise ValueError(
            "data size should be (n,2)"
        )

def process_series(df, index_column, series_name, series_type): # O(n)
    x, y = normalize(
        EWM(
            EWM(
                df[[index_column, series_name]],
                0.2
            ),
            0.5
        ),
        series_name
    )
    return Series(series_name, series_type, x, y)