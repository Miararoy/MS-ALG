def EWM(data, alpha: float):
    return data.ewm(alpha=alpha, min_periods=1).mean()