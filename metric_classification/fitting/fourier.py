from scipy.fftpack import fft, fftfreq

def fourier(data):
    ft = fft(data)
    freq = fftfreq(len(data))
    return ft, freq