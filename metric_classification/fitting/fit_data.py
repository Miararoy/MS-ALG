from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.metrics import mean_squared_error


def _sigmoid(x, a):
    return (a / (1 + np.exp((-1)* (x - 20))))

class Sigmoid(object):
    def __init__(self):
        self.sigmoid = _sigmoid
        self.coef_ = []
        self.popt = None
        self.pcov = None
    
    def fit(self, X, y):
        print("shape x = {}".format(X.shape))
        print("shape y = {}".format(y.shape))
        self.popt, self.pcov = curve_fit(self.sigmoid, X[:,0], y[:,0])
        self.coef_.append(self.popt)
        
    def predict(self, x):
        return pd.Series(x[:,0]).apply(lambda r: self.sigmoid(r, *self.popt))
        

def fit_linear(x, y):
    model = linear_model.LinearRegression()
    model.fit(x, y)
    _pred = model.predict(x)
    return model.coef_, mean_squared_error(y, _pred), _pred


def fit_sigomid(x, y):
    model = Sigmoid()
    model.fit(x, y)
    _pred = model.predict(x)
    return model.coef_, mean_squared_error(y, _pred), _pred